import http from "./axios-common";

const getAllPosts = () => {
  return http.get("/posts");
};

const getSinglePost = id => {
  return http.get(`/posts/${id}`);
};

const createPost = data => {
  return http.post("/posts", data);
};

const updateSinglePost = (id, data) => {
  return http.put(`/posts/${id}`, data);
};

const deleteSinglePost = id => {
  return http.delete(`/posts/${id}`);
};

const deleteAllPosts = () => {
  return http.delete("/posts");
};
const Posts = {
  createPost,
  getAllPosts,
  getSinglePost,
  updateSinglePost,
  deleteSinglePost,
  deleteAllPosts,
};

export default Posts;
