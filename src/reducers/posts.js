import {
  CREATE,
  VIEW,
  VIEW_ALL,
  DELETE,
  UPDATE,
} from "../actions/actionType";

const initialState = [];

const postsReducer = (posts = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE:
      return [...posts, payload];

    case VIEW_ALL:
      return payload;

    case VIEW:
      if (posts.id === payload.id) {
        return [...posts, ...payload];
      }

    case UPDATE:
      posts.map((post) => {
        if (post.id === payload.id) {
          return {...post, ...payload};
        } else {
          return post;
        }
      });

    case DELETE:
      posts.filter(({id}) => id !== payload.id);
      return [...posts]

    default:
        return posts;
  }
};

export default postsReducer;
