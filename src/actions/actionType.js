export const CREATE = "CREATE";
export const VIEW = "VIEW";
export const VIEW_ALL = "VIEW_ALL";
export const UPDATE = "UPDATE";
export const DELETE = "DELETE";