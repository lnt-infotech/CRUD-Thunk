import {
  CREATE,
  VIEW,
  VIEW_ALL,
  UPDATE,
  DELETE,
} from "./actionType";

import Posts from "../http";

export const createPost = (title, body) => async (dispatch) => {
  try {
    const res = await Posts.createPost({ title, body });

    dispatch({
      type: CREATE,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getAllPosts = () => async (dispatch) => {
    try{
        const res = await Posts.getAllPosts();

        dispatch({
            type: VIEW_ALL,
            payload: res.data
        })
        
        return Promise.resolve(res.data)
    }catch(err){
        return Promise.reject(err)
    }
};

export const getSinglePost = id => async (dispatch) => {
    try{
        const res = await Posts.getSinglePost(id);

        dispatch({
            type: VIEW,
            payload: res.data
        })

        return Promise.resolve(res.data)
    }catch(err){
        return Promise.reject(err)
    }
}

export const deleteSinglePost = id => async (dispatch) => {
    try{
        const res = await Posts.deleteSinglePost(id);

        dispatch({
            type: DELETE,
            payload: {id}
        })

        return Promise.resolve(res.data)
    }catch(err){
        return Promise.reject(err)
    }
}

export const updateSinglePost = (id, data) => async (dispatch) => {
    try{
        const res = await Posts.updateSinglePost(id, data)

        dispatch({
            type: UPDATE,
            payload: res.data
        })

        return Promise.resolve(res.data)
    }catch(err){
        return Promise.reject(err)
    }
}