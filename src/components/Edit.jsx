import React, { useEffect, useState } from "react";
import { connect, useDispatch } from "react-redux";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import { getSinglePost, updateSinglePost } from "../actions/posts";
// import Posts from "../http";
import NavBar from "./NavBar";

const Edit = () => {
  const initialState = {
    title: "",
    body: "",
  };

  const {id} = useParams()

  const [singlePost, setSinglePost] = useState(initialState);
  const [updatePost, setUpdatePost] = useState(false);

  const dispatch = useDispatch();

  const getPostSingle = id => {
    dispatch(getSinglePost(id)).then(data => {
      setSinglePost(data)
    })
  }

  const updateSinglePosts = () => {
    const data = {
      title: singlePost.title,
      body: singlePost.body
    }

    dispatch(updateSinglePost(singlePost.id, data))
    .then(res => {
      setSinglePost(res)
      setUpdatePost(true)
    })
  }

  useEffect(() => {
    getPostSingle(id)
  }, [id])

  return (
    <>
      <NavBar />
      <div className="container">
        <div className="row">
          <div className="col-md-12 mt-3">
            <h1 className="text-center">Edit a Post</h1>
          </div>
          <div className="col-md-6 offset-md-3">
            <label htmlFor="title" className="form-label">Title</label>
            <input
              className="form-control my-2"
              name="title"
              id="title"
              placeholder="Enter Title"
              required
              value={singlePost.title}
              onChange={(e) =>
                setSinglePost({ ...singlePost, [e.target.name]: e.target.value })
              }
            />
          </div>
          <div className="col-md-6 offset-md-3 mt-3">
            <label htmlFor="body" className="form-label">body</label>
            <input
              className="form-control my-2"
              name="body"
              id="body"
              placeholder="Add body"
              required
              value={singlePost.body}
              onChange={(e) =>
                setSinglePost({ ...singlePost, [e.target.name]: e.target.value })
              }
            />
          </div>
          <div className="col-md-6 offset-md-3 mt-3">
            <button
              className="btn btn-success btn-block"
              onClick={updateSinglePosts}
            >Update Post</button>
            <Link to="/"><button
              className="btn btn-secondary btn-block mx-2 px-2"
            >Back</button></Link>
          </div>
          {updatePost && (
            <div className="col-md-6 offset-md-3 mt-3">
              <div className="alert alert-success">Post updated successfully</div>
            </div>
          )}
        </div>
      </div>
    </>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    updateSinglePost: () => dispatch(updateSinglePost())
  }
}

export default connect(null, mapDispatchToProps)(Edit);