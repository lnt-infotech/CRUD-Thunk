import React, { useState } from "react";
import { connect, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { createPost } from "../actions/posts";
import NavBar from "./NavBar";

const Add = () => {
  const initialState = {
    id: null,
    title: "",
    body: "",
  };

  const [post, setPost] = useState(initialState);
  const [submitPost, setSubmitPost] = useState(false);

  const dispatch = useDispatch();

  const savePost = () => {
    const { title, body } = post;

    dispatch(createPost(title, body)).then((data) => {
      setPost({
        id: data.id,
        title: data.title,
        body: data.body,
      });
      setSubmitPost(true);
    });
  };

  return (
    <>
      <NavBar />
      <div className="container">
        <div className="row">
          <div className="col-md-12 mt-3">
            <h1 className="text-center">Add a Post</h1>
          </div>
          <div className="col-md-6 offset-md-3">
            <label htmlFor="title" className="form-label">
              Title
            </label>
            <input
              className="form-control my-2"
              name="title"
              id="title"
              placeholder="Enter Title"
              required
              onChange={(e) =>
                setPost({ ...post, [e.target.name]: e.target.value })
              }
            />
          </div>
          <div className="col-md-6 offset-md-3 mt-3">
            <label htmlFor="body" className="form-label">
              Body
            </label>
            <input
              className="form-control my-2"
              name="body"
              id="body"
              placeholder="Add Body"
              required
              onChange={(e) =>
                setPost({ ...post, [e.target.name]: e.target.value })
              }
            />
          </div>

          <div className="col-md-6 offset-md-3">
            <button
              className="btn btn-success btn-block"
              onClick={savePost}
              disabled={(post.title === "" || post.body === "") && true}
            >
              Add Post
            </button>
            <Link to="/">
              <button className="btn btn-secondary btn-block mx-2 px-2">
                Back
              </button>
            </Link>
          </div>
          {submitPost && (
            <div className="col-md-6 offset-md-3 mt-3">
              <div className="alert alert-success">Post added successfully</div>
            </div>
          )}
        </div>
      </div>
    </>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    createPost: () => dispatch(createPost()),
  };
};

export default connect(null, mapDispatchToProps)(Add);
