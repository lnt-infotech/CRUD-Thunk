import React, { useEffect, useState } from "react";
import NavBar from "./NavBar";
import { getAllPosts, deleteSinglePost } from "../actions/posts";
import { connect, useDispatch } from "react-redux";
import { Link } from "react-router-dom";

const Home = ({ posts, getAllPosts }) => {
  
  const dispatch = useDispatch();

  const deleteOne = (id) => {
    dispatch(deleteSinglePost(id)).then(() => {
      getAllPosts();
    });
  };

  useEffect(() => {
    getAllPosts();
  }, []);
  return (
    <>
      <NavBar />
      <div className="container">
        <div className="row">
          <div className="col-md-12 mt-3">
            <h1 className="text-center">All Posts</h1>
          </div>
          <table className="table table-striped table-dark">
            <thead className="text-center">
              <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Body</th>
                <th>Options</th>
              </tr>
            </thead>
            <tbody>
              {posts.length === 0 ? (
                <tr>
                  <td colSpan={4}>
                    <h4 className="text-center">No Posts Found</h4>
                  </td>
                </tr>
              ) : (
                posts.length > 0 &&
                posts.map((posts) => (
                  <tr key={posts.id}>
                    <td>{posts.id}</td>
                    <td>{posts.title}</td>
                    <td>{posts.body}</td>
                    <td>
                      <Link to={"/view/" + `${posts.id}`} className="px-2 mb-2">
                        <button className="btn btn-success btn-sm">View</button>
                      </Link>
                      <Link to={"/edit/" + `${posts.id}`} className="px-2 mb-2">
                        <button className="btn btn-warning btn-sm text-dark">
                          Edit
                        </button>
                      </Link>
                      <button
                        className="btn btn-danger btn-sm text-white"
                        onClick={() => deleteOne(posts.id)}
                      >
                        Delete
                      </button>
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    posts: state.posts,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAllPosts: () => dispatch(getAllPosts()),
    deleteSinglePost: () => dispatch(deleteSinglePost()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
