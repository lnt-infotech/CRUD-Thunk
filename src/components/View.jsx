import React, { useEffect, useState } from "react";
import { connect, useDispatch } from "react-redux";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import { getSinglePost } from "../actions/posts";
import NavBar from "./NavBar";

const View = () => {
  const initialState = {
    title: "",
    body: "",
  };

  const {id} = useParams()

  const [singlePost, setSinglePost] = useState(initialState);

  const dispatch = useDispatch();

  const getPostSingle = id => {
    dispatch(getSinglePost(id)).then((data) => {
      setSinglePost(data)
    })
  }

  useEffect(() => {
    getPostSingle(id)
  }, [id])

  return (
    <>
      <NavBar />
      <div className="container">
        <div className="row">
          <div className="col-md-12 mt-3">
            <h1 className="text-center">View a Post</h1>
          </div>
          <div className="col-md-6 offset-md-3">
            <p>Title: <strong>{singlePost.title}</strong></p>
            <p>Body: <strong>{singlePost.body}</strong></p>
          </div>
          <div className="col-md-6 offset-md-3 mt-3">
            <Link to="/"><button
              className="btn btn-secondary btn-block mx-2 px-2"
            >Back</button></Link>
          </div>
        </div>
      </div>
    </>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    getSinglePost: () => dispatch(getSinglePost())
  }
}

export default connect(null, mapDispatchToProps)(View);