import React from "react";
import { Link } from "react-router-dom";

const NavBar = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-danger px-2">
      <Link className="navbar-brand" to="/">
        Redux CRUD
      </Link>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          <Link className="nav-item nav-link active" to="/">
            Home
          </Link>
          <Link className="nav-item nav-link text-white" to="/add">
            Add
          </Link>
          <Link className="nav-item nav-link text-white" to="/show">
            Show All
          </Link>
          {/* <Link className="nav-item nav-link text-white" to="/calls">
            Calls
          </Link> */}
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
