import React from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Add from './components/Add';
import Home from './components/Home';
import Edit from './components/Edit';
import View from './components/View';
import ViewAll from './components/ViewAll';

function App() {
  return (
    <Router>
      <Routes>
        <Route path='' element={<Home />} />
        <Route path='add' element={<Add />} />
        <Route path='edit/:id' element={<Edit />} />
        <Route path='view/:id' element={<View />} />
        <Route path='show' element={<ViewAll />} />
      </Routes>
    </Router>
  );
}

export default App;
